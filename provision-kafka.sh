#!/usr/bin/env bash

# Uncomment on first run, then just comment off
#sudo apt-get update
#sudo apt-get install -y openjdk-7-jre-headless

if [ ! -d /tmp/zookeeper ]; then
    echo creating zookeeper data dir...
    mkdir /tmp/zookeeper
    echo $1 > /tmp/zookeeper/myid
fi

# Uncomment on first run, then just comment off
#cp -R /vagrant/kafka_2.11-0.10.0.0 kafka
cd kafka
bin/zookeeper-server-start.sh /vagrant/config/zookeeper.properties > /tmp/zookeeper.log &
bin/kafka-server-start.sh /vagrant/config/server-$1.properties > /tmp/kafka.log &