# README #

Short Vagrant script for setting up and running a 3 VM cluster, each running Kafka and Zookeeper. 

Check the provision script (`provision-kafka.sh`) - it has some rows that need to be uncommented for the first run and commented out after that (otherwise all the deps get reinstalled every time you restart the VM).